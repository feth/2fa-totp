{% set osconfig = salt['grains.filter_by']({
    'Debian': {
      'insert_position': '@include common-auth',
      'comment': '^@include common-auth',
      'package': 'libpam-google-authenticator',
    },
    'RedHat': {
      'insert_position': 'auth       include      postlogin',
      'comment': '^auth       substack     password-auth',
      'package': 'google-authenticator',
    },
  },
  default='Debian',
  grain='os_family')
%}

{% if grains.os_family == "RedHat" %}
epel-release-for-gg-auth-totp:
  pkg.installed:
    - name: epel-release

{{ osconfig.package }}:
  pkg.installed:
    - require:
      - pkg: epel-release
{% else %}
{{ osconfig.package }}:
  pkg.installed
{% endif %}


sshd_pam_googleauth:
  file.keyvalue:
    - name: "/etc/ssh/sshd_config"
    - separator: " "
    - append_if_not_found: True
    - key_ignore_case: True
    - key_values:
        UsePam: "yes"
        ChallengeResponseAuthentication: "yes"
        PasswordAuthentication: "{{ salt['pillar.get']('ssh_allow_password', default="no") }}"
        AuthenticationMethods: "publickey,keyboard-interactive"
        KbdInteractiveAuthentication: "yes"
  service.running:
    - name: sshd
    - watch:
      - file: sshd_pam_googleauth

{% if grains.os == "Debian"  and grains.osmajorrelease < 10 %}
{% set prompt_supported = False %}
{% else %}
{% set prompt_supported = True %}
{% endif %}


pam_ssh_google:
  file.blockreplace:
    - name: "/etc/pam.d/sshd"
    - insert_after_match: "{{ osconfig.insert_position }}"
    - marker_start: "# START managed zone googletotp -DO-NOT-EDIT-"
    - marker_end: "# END managed zone googletotp --"
    - content: |
        # Make sure the counter (for HOTP mode) isn't incremented for failed attempts.
        # nullok: Allow users to log in if their secret files don't exist
        auth required pam_google_authenticator.so no_increment_hotp nullok{% if prompt_supported %} [authtok_prompt=Jeton TOTP: ]{% endif %}
        auth required pam_permit.so
    - show_changes: True

pam_ssh_google2:
  file.comment:
    - name: "/etc/pam.d/sshd"
    - regex: {{ osconfig.comment }}
