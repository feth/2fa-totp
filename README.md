# 2FA-totp - salt formula

use google totp lib with pam

Testing is done with debian bullseye and centos7 ; should work with most distros ; tell me :)

I don't use TOTP with google authenticator: there are many alternatives. No, there is no interaction with the cloud nor google.
Search for tools implementing [RFC 6238](https://datatracker.ietf.org/doc/html/rfc6238).

# Every user may setup the 2FA with TOTP

```google-authenticator```

-> Will create a .google-authenticator file.

# Behaviour

Users with a .google-authenticator file are prompted for their token. Other users can login without.
Options in the pam module allow to make the TOTP compulsory if you want.
